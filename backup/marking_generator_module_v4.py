import openpyxl
import docx
import openpyxl
from datetime import datetime
from openpyxl.styles.borders import Border, Side

# Global variables related to file or gui state

# This stores the coordinates of the current scanned matrix (TFMC only)
global table_coordinates
table_coordinates = []

# This stores the matrix table currently being viewed (TFMC only)
global table_coor_index
table_coor_index = 1

# This stores information from the heading of the current matrix file.
global matrixHeading
matrixHeading = []

# These global variables hold the content for to view in the listwidget on screen and to import in the excel marking.
global visible_columns
visible_columns = []

global hidden_columns
hidden_columns = []

# Store the name and path of the file you wish to load.
global filename
filename = ''


from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QMainWindow, QApplication, QWidget, QAction, QTableWidget, QTableWidgetItem, QVBoxLayout, \
    QMessageBox, QDialog, QPushButton, QInputDialog, QLineEdit, QFormLayout, QFileDialog




filename = ''
import logging
logging.basicConfig(filename='app.log', filemode='a', format= '%(asctime)s - %(name)s - %(lineno)d - %(levelname)s - %(message)s')

import win32com.client
from pathlib import Path



global wb_marking_template
wb_marking_template = openpyxl.load_workbook('markingTemplate2.xlsx')

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(690, 916)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.horizontalLayout_7 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_7.setObjectName("horizontalLayout_7")
        self.checkBox_3 = QtWidgets.QCheckBox(self.centralwidget)
        self.checkBox_3.setObjectName("checkBox_3")
        self.horizontalLayout_7.addWidget(self.checkBox_3)
        self.checkBox_4 = QtWidgets.QCheckBox(self.centralwidget)
        self.checkBox_4.setObjectName("checkBox_4")
        self.horizontalLayout_7.addWidget(self.checkBox_4)
        self.gridLayout.addLayout(self.horizontalLayout_7, 10, 0, 1, 3)
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setObjectName("pushButton")
        self.horizontalLayout_6.addWidget(self.pushButton)
        self.gridLayout.addLayout(self.horizontalLayout_6, 0, 0, 1, 3)
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setObjectName("label")
        self.verticalLayout_2.addWidget(self.label)
        self.listWidget = QtWidgets.QListWidget(self.centralwidget)
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(0, 195, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 195, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.PlaceholderText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 195, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 195, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.PlaceholderText, brush)
        brush = QtGui.QBrush(QtGui.QColor(120, 120, 120))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.PlaceholderText, brush)
        self.listWidget.setPalette(palette)
        self.listWidget.setObjectName("listWidget")
        item = QtWidgets.QListWidgetItem()
        self.listWidget.addItem(item)
        item = QtWidgets.QListWidgetItem()
        self.listWidget.addItem(item)
        item = QtWidgets.QListWidgetItem()
        self.listWidget.addItem(item)
        item = QtWidgets.QListWidgetItem()
        self.listWidget.addItem(item)
        item = QtWidgets.QListWidgetItem()
        self.listWidget.addItem(item)
        item = QtWidgets.QListWidgetItem()
        self.listWidget.addItem(item)
        item = QtWidgets.QListWidgetItem()
        self.listWidget.addItem(item)
        item = QtWidgets.QListWidgetItem()
        self.listWidget.addItem(item)
        item = QtWidgets.QListWidgetItem()
        self.listWidget.addItem(item)
        item = QtWidgets.QListWidgetItem()
        self.listWidget.addItem(item)
        item = QtWidgets.QListWidgetItem()
        self.listWidget.addItem(item)
        self.verticalLayout_2.addWidget(self.listWidget)
        self.horizontalLayout_5.addLayout(self.verticalLayout_2)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setObjectName("label_2")
        self.verticalLayout.addWidget(self.label_2)
        self.listWidget_2 = QtWidgets.QListWidget(self.centralwidget)
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(170, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(170, 0, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.PlaceholderText, brush)
        brush = QtGui.QBrush(QtGui.QColor(170, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(170, 0, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.PlaceholderText, brush)
        brush = QtGui.QBrush(QtGui.QColor(120, 120, 120))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.PlaceholderText, brush)
        self.listWidget_2.setPalette(palette)
        font = QtGui.QFont()
        font.setStrikeOut(True)
        self.listWidget_2.setFont(font)
        self.listWidget_2.setObjectName("listWidget_2")
        item = QtWidgets.QListWidgetItem()
        self.listWidget_2.addItem(item)
        item = QtWidgets.QListWidgetItem()
        self.listWidget_2.addItem(item)
        self.verticalLayout.addWidget(self.listWidget_2)
        self.horizontalLayout_5.addLayout(self.verticalLayout)
        self.gridLayout.addLayout(self.horizontalLayout_5, 1, 0, 1, 3)
        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(0, 165, 180))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 255, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 170, 127))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.ToolTipText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 255, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.PlaceholderText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 165, 180))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 255, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 170, 127))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.ToolTipText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 255, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.PlaceholderText, brush)
        brush = QtGui.QBrush(QtGui.QColor(120, 120, 120))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(120, 120, 120))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 170, 127))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.ToolTipText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.PlaceholderText, brush)
        self.label_5.setPalette(palette)
        self.label_5.setObjectName("label_5")
        self.gridLayout.addWidget(self.label_5, 13, 0, 1, 3)
        spacerItem = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        self.gridLayout.addItem(spacerItem, 14, 1, 1, 1)
        spacerItem1 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        self.gridLayout.addItem(spacerItem1, 12, 0, 1, 1)
        self.label_21 = QtWidgets.QLabel(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_21.sizePolicy().hasHeightForWidth())
        self.label_21.setSizePolicy(sizePolicy)
        self.label_21.setText("")
        self.label_21.setPixmap(QtGui.QPixmap("../PYQTTESTS/logo.png"))
        self.label_21.setAlignment(QtCore.Qt.AlignCenter)
        self.label_21.setObjectName("label_21")
        self.gridLayout.addWidget(self.label_21, 15, 0, 1, 3)
        spacerItem2 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        self.gridLayout.addItem(spacerItem2, 8, 2, 1, 1)
        self.line = QtWidgets.QFrame(self.centralwidget)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.gridLayout.addWidget(self.line, 4, 0, 1, 3)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setObjectName("label_3")
        self.horizontalLayout_3.addWidget(self.label_3)
        self.lineEdit_4 = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_4.setObjectName("lineEdit_4")
        self.horizontalLayout_3.addWidget(self.lineEdit_4)
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setObjectName("label_4")
        self.horizontalLayout_3.addWidget(self.label_4)
        self.lineEdit_5 = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_5.setObjectName("lineEdit_5")
        self.horizontalLayout_3.addWidget(self.lineEdit_5)
        self.pushButton_5 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_5.setObjectName("pushButton_5")
        self.horizontalLayout_3.addWidget(self.pushButton_5)
        self.gridLayout.addLayout(self.horizontalLayout_3, 5, 0, 1, 3)
        spacerItem3 = QtWidgets.QSpacerItem(20, 10, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        self.gridLayout.addItem(spacerItem3, 6, 2, 1, 1)
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.pushButton_6 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_6.setObjectName("pushButton_6")
        self.horizontalLayout_4.addWidget(self.pushButton_6)
        self.pushButton_7 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_7.setObjectName("pushButton_7")
        self.horizontalLayout_4.addWidget(self.pushButton_7)
        self.pushButton_8 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_8.setObjectName("pushButton_8")
        self.horizontalLayout_4.addWidget(self.pushButton_8)
        self.pushButton_2 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_2.setObjectName("pushButton_2")
        self.horizontalLayout_4.addWidget(self.pushButton_2)
        self.pushButton_4 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_4.setObjectName("pushButton_4")
        self.horizontalLayout_4.addWidget(self.pushButton_4)
        self.gridLayout.addLayout(self.horizontalLayout_4, 2, 0, 1, 3)
        spacerItem4 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        self.gridLayout.addItem(spacerItem4, 3, 2, 1, 1)
        self.line_2 = QtWidgets.QFrame(self.centralwidget)
        self.line_2.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_2.setObjectName("line_2")
        self.gridLayout.addWidget(self.line_2, 9, 0, 1, 3)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.lineEdit = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit.setText("")
        self.lineEdit.setObjectName("lineEdit")
        self.horizontalLayout.addWidget(self.lineEdit)
        self.lineEdit_2 = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_2.setText("")
        self.lineEdit_2.setObjectName("lineEdit_2")
        self.horizontalLayout.addWidget(self.lineEdit_2)
        self.lineEdit_6 = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_6.setObjectName("lineEdit_6")
        self.horizontalLayout.addWidget(self.lineEdit_6)
        self.gridLayout.addLayout(self.horizontalLayout, 7, 0, 1, 3)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.pushButton_3 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_3.setObjectName("pushButton_3")
        self.horizontalLayout_2.addWidget(self.pushButton_3)
        self.gridLayout.addLayout(self.horizontalLayout_2, 11, 0, 1, 3)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 690, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        self.pushButton.clicked.connect(self.open_dialog_box)
        self.listWidget.itemDoubleClicked.connect(self.column_hide)
        self.listWidget_2.itemDoubleClicked.connect(self.column_show)
        self.pushButton_5.clicked.connect(self.add__marking_item)
        self.pushButton_6.clicked.connect(self.move_item_up)
        self.pushButton_7.clicked.connect(self.move_item_down)
        self.pushButton_3.clicked.connect(self.export_to_excel_skilt)
        self.pushButton_2.clicked.connect(self.prev_part)
        self.pushButton_4.clicked.connect(self.next_part)
        self.pushButton_8.clicked.connect(self.edit_value)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Marking Generator"))
        self.checkBox_3.setText(_translate("MainWindow", "Generate PDF only for customer review"))
        self.checkBox_4.setText(_translate("MainWindow", "AKER (Upcoming feature)"))
        self.pushButton.setText(_translate("MainWindow", "Scan specification matrix "))
        self.label.setText(_translate("MainWindow", "Included (Double click to exclude)"))
        __sortingEnabled = self.listWidget.isSortingEnabled()
        self.listWidget.setSortingEnabled(False)
        item = self.listWidget.item(0)
        item.setText(_translate("MainWindow", "Customer:    Dana Petroleum\n"
""))
        item = self.listWidget.item(1)
        item.setText(_translate("MainWindow", "Customer PO.:    616452\n"
""))
        item = self.listWidget.item(2)
        item.setText(_translate("MainWindow", "GA Drwg :    10168417\n"
""))
        item = self.listWidget.item(3)
        item.setText(_translate("MainWindow", "Probe  length :    65mm\n"
""))
        item = self.listWidget.item(4)
        item.setText(_translate("MainWindow", "Max test pressure: 1035 Bar\n"
""))
        item = self.listWidget.item(5)
        item.setText(_translate("MainWindow", "Cal. Pressure range :0-345 Bara\n"
""))
        item = self.listWidget.item(6)
        item.setText(_translate("MainWindow", "Mat Spec:API 6A/17D. PSL 3 PR2\n"
""))
        item = self.listWidget.item(7)
        item.setText(_translate("MainWindow", "Mat.Temp. Class: HH  / LU\n"
""))
        item = self.listWidget.item(8)
        item.setText(_translate("MainWindow", "Flange/Ring Gasket: API 6A 2 1/16” 10K BX-152\n"
""))
        item = self.listWidget.item(9)
        item.setText(_translate("MainWindow", "Date of Manufacture: xx.xx.xxxx\n"
""))
        item = self.listWidget.item(10)
        item.setText(_translate("MainWindow", "New Item"))
        self.listWidget.setSortingEnabled(__sortingEnabled)
        self.label_2.setText(_translate("MainWindow", "Excluded (Double click to include)"))
        __sortingEnabled = self.listWidget_2.isSortingEnabled()
        self.listWidget_2.setSortingEnabled(False)
        item = self.listWidget_2.item(0)
        item.setText(_translate("MainWindow", "Customer PN :    \n"
"NA"))
        item = self.listWidget_2.item(1)
        item.setText(_translate("MainWindow", "Cal. Temp. range:\n"
"NA"))
        self.listWidget_2.setSortingEnabled(__sortingEnabled)
        self.label_5.setText(_translate("MainWindow", "Console: "))
        self.label_3.setText(_translate("MainWindow", "Label"))
        self.lineEdit_4.setPlaceholderText(_translate("MainWindow", "Enter the name of an additional optional line"))
        self.label_4.setText(_translate("MainWindow", "value"))
        self.lineEdit_5.setPlaceholderText(_translate("MainWindow", "Enter the corresponding value"))
        self.pushButton_5.setText(_translate("MainWindow", "Add"))
        self.pushButton_6.setText(_translate("MainWindow", "Move item up"))
        self.pushButton_7.setText(_translate("MainWindow", "Move item down"))
        self.pushButton_2.setText(_translate("MainWindow", "Previous part"))
        self.pushButton_4.setText(_translate("MainWindow", "Next part"))
        self.lineEdit.setPlaceholderText(_translate("MainWindow", "Enter Serial number  (optional)"))
        self.lineEdit_2.setPlaceholderText(_translate("MainWindow", "Enter date of prodution (Optional)"))
        self.lineEdit_6.setPlaceholderText(_translate("MainWindow", "Enter PO number (optional)"))
        self.pushButton_3.setText(_translate("MainWindow", "Generate marking"))
        self.pushButton_8.setText(_translate("MainWindow", "Edit"))

        self.listWidget.clear()
        self.listWidget_2.clear()


    def edit_value(self):

        global visible_columns

        try:
            item_to_edit = self.listWidget.currentItem().text()
            index_item_to_edit = visible_columns.index(item_to_edit)

            ex = inputdialogdemo()
            exInput = ex.gettext()
            print(exInput)
            # Get the old label.
            old_text = visible_columns[index_item_to_edit].split('//')
            new_text = old_text[0] + ' // ' + exInput

            visible_columns[index_item_to_edit] = new_text

            self.update_columns()

        except:
            print('No value selected')


    # This function is used for matrices with multiple parts.
    def next_part(self):

        global table_coor_index
        global table_coordinates

        if table_coor_index < len(table_coordinates)-1:
            table_coor_index += 1
            print('Moved to part ' + str(table_coor_index) + 'of '+ str(len(table_coordinates)))
            self.label_5.setText('Moved to part ' + str(table_coor_index) + 'of '+ str(len(table_coordinates)))
            self.find_keywords()

        else:
            self.label_5.setText('This is the last or only part in this matrix.')

    # This function is used for matrices with multiple parts.
    def prev_part(self):

        global table_coor_index
        global table_coordinates

        if table_coor_index > 1:
            table_coor_index -= 1
            self.label_5.setText('Moved to part ' + str(table_coor_index) + 'of ' + str(len(table_coordinates)))
            self.find_keywords()

        else:
            self.label_5.setText('This is the first or only part in this matrix.')

    #-------------------------------------------

    # Open a dialogue box to find the matrix you wish to import.
    def open_dialog_box(self):

        try:
            file_name = QFileDialog.getOpenFileName()
            # Path is the first item in the list returned by above function, rest is debug info (I think)
            path = file_name[0]
            #print(path)
            #print(file_name)

            # Convert any 'doc' documents to 'docx' documents in the current directory.
            path_list = path.split('/')
            #print(path_list)
            path_list.pop()

            path_string = ''
            for i in path_list:
                path_string += i
                path_string += ('\\')

            #print(path_string)

            if '.doc' in path:
                if '.docx' not in path:
                    self.docx_converter(
                        comObject='Word.application',
                        dirPath=Path(path_string),
                        sourceExtension='doc',
                        destinationExtension='docx',
                        fileFormat=16,
                        dryRun=False
                    )
                    path += 'x'
                    #print(path)

            # run rest of program after converting doc to docx

            global filename
            filename = path

            # This function finds all the keywords in the matrix and updates the two global column lists with them.
            self.find_keywords()

        except:
            print('no valid file selected')


    def warning_message(self, text, title):
        msg = QMessageBox()
        msg.setText(text)
        msg.setIcon(QMessageBox.Information)
        msg.setWindowTitle("Possible issue with matrix")
        returnValue = msg.exec()
        if returnValue == QMessageBox.Ok:
            print('OK clicked')



    # Function to convert doc files into docx files. Used for SDP-6 matrices which typically are in .doc format which
    # python docx lib cannot read.
    def docx_converter(self, comObject, dirPath, sourceExtension, destinationExtension, fileFormat, dryRun=False):
        # This function converts all .doc files in the path folder to .docx files.
        msApp = win32com.client.Dispatch(comObject)

        for sourceFile in sorted(dirPath.rglob(f'*.{sourceExtension}')):

            destinationFile = sourceFile.with_suffix(f'.{destinationExtension}')

            if not destinationFile.is_file():

                print(f'Converting: {sourceFile}')

                if not dryRun:

                    try:

                        if comObject == 'Excel.application':

                            doc = msApp.Workbooks.Open(str(sourceFile))

                        elif comObject == 'Word.application':

                            doc = msApp.Documents.Open(str(sourceFile))

                        doc.SaveAs(str(destinationFile), FileFormat=fileFormat)

                        doc.Close()

                    except Exception as e:

                        print(f'Failed to Convert: {sourceFile} : {e}')

        msApp.Quit()

        return

    # Function used to separate a file with multiple parts in sections stored as begin and end coordinates for cells.
    def find_coordinates_instrument_datasheet(self):

        global filename
        global table_coordinates
        table_coordinates = []

        self.label_5.setText('File: ' + filename + ' succesfully imported, scanned marking keywords.')
        doc = docx.Document(filename)
        tables = doc.tables

        allCells = []
        found_words = []

        for table in tables:
            for row in table.rows:
                for cell in row.cells:
                    allCells.append(cell.text)
                    # print(row.cells[])

        # print(allCells)

        # Locate the coordinates of each table so we can cycle through tables.

        global last_coor
        last_coor = 0
        global new_coor
        new_coor = 0

        # Only read after the last pages of the datasheet. Usuallly surface protection is a unique item at the end
        # of the datasheet so we use this to trigger that we are now at the end and ready to start reading.
        start_read = False
        for i in range(0,len(allCells)):
            if 'Surface' in allCells[i]:
                start_read = True

            if start_read == True:

                # Supplier is the first word in the marking table and occurs 6 times because it is a merged cell.
                if 'Supplier' in allCells[i]:
                    #print(allCells[i])
                    new_coor = i

                    # If 10 table cells have passed since the last time we detected the word 'supplier', assume that
                    # next time we encounter it, it is a new start of a table.

                    if last_coor +10 < new_coor:
                        coordinates_to_append = [last_coor, new_coor]
                        table_coordinates.append(coordinates_to_append)
                        last_coor = new_coor

        # The last item does not have a next item to fence it off, so we add it manually.
        table_coordinates.append([last_coor, last_coor+50])
        #print(table_coordinates)

    def find_keywords_instrument_datasheet(self, coordinates):

        global datasheet_keyWords
        datasheet_keyWords = [
            'Serial No.:',
            'Customer PN :',
            'Customer PN:',
            'GA Drwg :',
            'GA Drwg:',
            'Sensor Type',
            'Probe  length :',
            'Probe  length:',
            'Cal/Work. Pressure range:',
            'Cal. Temp. range:',
            'Max test pressure:',
            'Mat. Class/Temp:',
            'Mat. Class/Temp: ',
            'Mat Spec',
            'Mat Spec:',
            'Flange/Ring Gasket',
            'Communications protocol:',
        ]

        keyWordsSingleCol_datasheet = [

            'Communications protocol:',
        ]

        #Todo incorporate legal character search.
        keyWords_legal_characters_only = []
        for i in datasheet_keyWords:
            i_legal = self.remove_all_none_alphanumeric_and_upper(i)
            if i_legal not in keyWords_legal_characters_only:
                keyWords_legal_characters_only.append(i_legal)

        keyWords_singelecol_legal_characters_only = []
        for i in keyWordsSingleCol_datasheet:
            i_legal = self.remove_all_none_alphanumeric_and_upper(i)
            if i_legal not in keyWords_singelecol_legal_characters_only:
                keyWords_singelecol_legal_characters_only.append(i_legal)


        self.label_5.setText('File: ' + filename + ' Reading partnumber x from instrument datasheet.')
        logging.info('succesfully loaded: ', filename )
        doc = docx.Document(filename)
        tables = doc.tables

        allCells = []
        found_words = []

        for table in tables:
            for row in table.rows:
                for cell in row.cells:
                    allCells.append(cell.text)
                    # print(cell.text)

        for i in range (coordinates[0], coordinates[1]):
            unique_value = True
            if allCells[i] in datasheet_keyWords:
                #print('FOUND:', allCells[i])
                # If not first part of a merged cell
                if allCells[i] != allCells[i+1]:
                    # This is wrong in the table so we correct
                    if allCells[i] == 'Serial No.:':
                        keyword_value_tuple = ('Sensor type', ' // ', allCells[i + 1])
                    else:
                        keyword_value_tuple = (allCells[i], ' // ', allCells[i + 1])

                    found_words.append(keyword_value_tuple)

        #print(found_words)
        return found_words

    # ----------------------------------------------------- Above only for TFMC, Below function that actually finds keywords
    def find_keywords(self):

        global visible_columns
        global hidden_columns

        visible_columns = []
        hidden_columns = []

        global filename

        # if it is a datahseet template instead of a spec matrix template, treat it differently. TFMC ONLY.
        if 'DATASHEET TFMC' in filename:
            print('file was a datasheet')
            self.label_5.setText('Instrument data sheet loaded.')
            self.find_coordinates_instrument_datasheet()
            # Todo: i think function below does not need to be there is it also runs on line 637
            self.find_keywords_instrument_datasheet(table_coordinates[table_coor_index])

        # Get information from the word file header
        doc = docx.Document(filename)
        headerCells = []
        section = doc.sections[0]
        header = section.header
        for table in header.tables:
            for row in table.rows:
                for cell in row.cells:
                    if (cell.text != '') and (cell.text != ':'):
                        headerCells.append(cell.text)
                    # print(row.cells[])
        print(headerCells)

        global matrixHeading
        matrixHeading = []

        matrixHeading.append(headerCells[1])
        matrixHeading.append(headerCells[5])
        matrixHeading.append(headerCells[7])
        # This removes any newline characters that may be in the doc number


        new_text = ''
        for i in matrixHeading[0]:
            if i.isnumeric:
                new_text += i
        print(new_text)
        new_text.replace('\t', '')
        matrixHeading.pop(0)
        matrixHeading.append(new_text)
        print(matrixHeading[0])
        print(matrixHeading)


        try:

            # if not a datasheet type template containing many markings
            found_words = []

            if 'DATASHEET TFMC' not in filename:

                self.label_5.setText('File: ' + filename + ' succesfully imported, scanned marking keywords.')
                doc = docx.Document(filename)
                tables = doc.tables
                global keyWords
                keyWords = ['Siemens Type product:',
                            'Client Name:',
                            'Customer PN',
                            'Customer PN:',
                            'Siemens Drawing nr.',
                            'Siemens Drawing nr.:',
                            'Siemens Drawing nr:',
                            'CR / Span',
                            'URL / FS',
                            'Probe length',
                            'Max test pressure',
                            'Max Test Pressure',
                            'Calibrated Pressure Range',
                            'Pressure Calibration Temperature Range',
                            'Calibrated Temperature Range',
                            'Mat Spec',
                            'API 6A Mat/Temp Class',
                            'Process interface',
                            'Protocol Address',
                            'Electrical Output, Communication protocol',
                            'Cable direction ',
                            'Cable direction'
                            'test pressure',
                            'Test Pressure ',
                            'Test Pressure',
                            ' PROBE LENGTH ',
                            ]


                # These keywords are always in the header of a Matrix.
                keyWordsSingleCol = ['Siemens Type product:',
                                     'Client Name:',
                                     'Customer PN',
                                     'Customer PN:',
                                     'Siemens Drawing nr.:',
                                     'Siemens Drawing nr.',]

                keyWords_legal_characters_only = []
                for i in keyWords:
                    i_legal = self.remove_all_none_alphanumeric_and_upper(i)
                    if i_legal not in keyWords_legal_characters_only:
                        keyWords_legal_characters_only.append(i_legal)

                keyWords_singelecol_legal_characters_only = []
                for i in keyWordsSingleCol:
                    i_legal = self.remove_all_none_alphanumeric_and_upper(i)
                    if i_legal not in keyWords_singelecol_legal_characters_only:
                        keyWords_singelecol_legal_characters_only.append(i_legal)

                # Load every single table cell of the matrix in a very long list.
                allCells = []
                for table in tables:
                    for row in table.rows:
                        for cell in row.cells:
                            allCells.append(cell.text)

                            # print(row.cells[])
                #print(allCells)




                # Loop through every single Matrix cell and check: a) is this a keyword i need, b) do I already have it?
                for i in allCells:
                    #print(i)
                    #print('-------------------')

                    unique_value = True

                    # Surface is usually the last table of the matrix, if this is reached, stop reading.
                    # The purpose is to prevent reading keywords at the bottom of the file which may have similar keywords.
                    if 'Surface' in i:
                        break

                    # Strip the keyword if all none-letter or numeric values.
                    i_legal_characters_only = self.remove_all_none_alphanumeric_and_upper(i)
                    print('word:',i_legal_characters_only, ',   ', keyWords_legal_characters_only)

                    if i_legal_characters_only in keyWords_legal_characters_only:

                        # Check if we already found a value. We only want the first label and value, no copies.
                        # Also check if perhaps a version exists with or without colon.

                        for j in found_words:

                            #print(i_legal_characters_only)
                            # Found words is a list of tuples consisting of three items. J[0] is the keyword followed by
                            # '//' and the value. Here we check if the keyword we find has been found before,
                            # Possibly with the colon at a different location.
                            if i_legal_characters_only +':' == j[0]:
                                unique_value = False
                            # elif i.replace(':', '') == j[0]:
                            #     unique_value = False
                            # elif i + ':' == j[0]:
                            #     unique_value = False

                        # If the keyword had not been found earlier in the document, take it's index.
                        if unique_value == True:
                            #print('FOUND: ',i)
                            # If the keyword is part of the table on page 1, the value is at the +1 index as this
                            # tabel only has 2 columns.

                            index = allCells.index(i)+1

                            # If the item is not part of the 2 column table on the first page of the doc,
                            # Then the keyword is on colomn 1, while the value is always in column 3, so add 2 to index.
                            if i_legal_characters_only not in keyWords_singelecol_legal_characters_only:
                                index = allCells.index(i) + 2
                                # Some word files have a weird bug where index + 1 and index + 2 exist twice even
                                # though the word document only visualises once. When this happens, go to +3.
                                if allCells[index] == allCells[index-1]:
                                    index = allCells.index(i) + 3

                            keyword_value_tuple = (i_legal_characters_only +':', ' // ', allCells[index])

                            # A separate set of rules for SDP-5 as the keyword is ugly in the matrix.

                            try:
                                #print(found_words[3][2])
                                if ('SDP' in found_words[3][2]):
                                    if i == 'URL / FS':
                                        keyword_value_tuple = ('MAX DP RANGE (URL):', ' // ', allCells[index])
                                    elif i == 'CR / Span':
                                        keyword_value_tuple = ('DP CAL. PRESSURE RANGE:', ' // ', allCells[index])

                                    # skip to the next for loop item as SPD-5 process interface is not part of SDP-5 marking
                                    elif i == 'Process interface':
                                        continue

                            except:
                                print('Product type unknown')

                            # A separate set of rules for WEPS to remove some keywords not commonly used for WEPS.
                            try:
                                #print(found_words[3][2])
                                if ('SDP' not in found_words[3][2]):
                                    #print('This is a WEPS SENSOR, applying WEPS formatting')
                                    if i_legal_characters_only == 'CABLE DIRECTION':
                                        continue
                                    elif i_legal_characters_only == 'ELECTRICAL OUTPUT COMMUNICATION PROTOCOL':
                                        continue

                            except:
                                print('Product type unknown')



                            # Found words is a list of all the keywords we found + '//' + corresponding value.
                            found_words.append(keyword_value_tuple)
                # print(found_words)

            else:
                found_words = self.find_keywords_instrument_datasheet(table_coordinates[table_coor_index])

            self.listWidget.clear()

            print(found_words)
            visible_columns = []
            for i in found_words:
                # convert the tuples in to a list of strings.
                visible_columns.append(str(i[0] + i[1] + i[2]))

            print(visible_columns)

            self.update_columns()

        except Exception as e:
            print(e, ' // no valid file selected')
            logging.exception('exception occured -', filename, ' - failed to read')
            self.label_5.setText('e, // No compatible word specification matrix file was loaded, only docx format supported.')

    # ------------------ Operations on found keywords

    def column_hide(self):

        global visible_columns
        global hidden_columns

        try:
            list_item = self.listWidget.currentItem().text()
            print(list_item)

            visible_columns.remove(list_item)
            hidden_columns.append(list_item)

        except AttributeError:
            print('hide; no item selected')

        self.update_columns()

    def column_show(self):

        global visible_columns
        global hidden_columns

        try:
            list_item = self.listWidget_2.currentItem().text()
            print(list_item)

            visible_columns.append(list_item)
            hidden_columns.remove(list_item)


        except AttributeError:
            print('Show, no item selected')

        self.update_columns()

    def update_columns(self):

        global visible_columns
        global hidden_columns

        self.listWidget.clear()
        self.listWidget_2.clear()

        self.listWidget.addItems(visible_columns)
        self.listWidget_2.addItems(hidden_columns)

    def add__marking_item(self):
        item_to_add = []
        item_to_add.append(self.lineEdit_4.text())
        item_to_add.append(' // ')
        item_to_add.append(self.lineEdit_5.text())

        visible_columns.append(str(item_to_add[0] + item_to_add[1] + item_to_add[2]))
        self.update_columns()

    def move_item_up(self):

        try:
            item_to_move = self.listWidget.currentItem().text()
            index_item_to_move = visible_columns.index(item_to_move)

            if index_item_to_move > 0:
                new_index_item_to_move = index_item_to_move - 1
                visible_columns.pop(index_item_to_move)
                visible_columns.insert(new_index_item_to_move, item_to_move)
                self.update_columns()
                #self.listWidget.setCurrentItem(visible_columns[2])
                self.listWidget.setCurrentRow(new_index_item_to_move)

            else:
                print('operation move failed because item on top or bottom')

        except AttributeError:
            print('no item selected')

    def move_item_down(self):

        try:
            item_to_move = self.listWidget.currentItem().text()
            index_item_to_move = visible_columns.index(item_to_move)


            new_index_item_to_move = index_item_to_move + 1
            visible_columns.pop(index_item_to_move)
            visible_columns.insert(new_index_item_to_move, item_to_move)
            self.update_columns()
            self.listWidget.setCurrentRow(new_index_item_to_move)


        except AttributeError:
            print('no item selected or item already at bottom')

    # --------------- Export functions (not that all are now combined to one. The skilt functions triggers all.
    # purge all illegal characters.
    def remove_illegal_characters(self, string):
        legal_string = ''

        for i in string:

            if i.isalpha():
                legal_string += i.lower()

            elif i.isnumeric():
                legal_string += i.lower()

            else:
                legal_string += '-'

        return legal_string

    def remove_all_none_alphanumeric_and_upper(self, string):
        # Todo remove spaces at beginning and end
        legal_string = ''
        loop_counter = 0

        for i in string:

            if i.isalpha():
                legal_string += i.upper()

            elif i.isnumeric():
                legal_string += i.upper()

            # Use only spaces between words. Not spaces in beginning or end of the cell content.
            elif i.isspace():
                if (loop_counter > 2) and (loop_counter < (len(string) -2)):
                    legal_string += i.upper()
            loop_counter += 1
        return legal_string

    def export_to_excel_marking(self):

        global visible_columns

        excel_column1 = []
        excel_column2 = []
        PO_number = self.remove_illegal_characters(self.lineEdit_6.text())

        for i in visible_columns:
            j = i.split('//')

            # The layout in spec matrix sometimes lacks ':' which looks shitty
            if ':' not in j[0]:
                j[0] += ':'

            excel_column1.append(j[0])
            excel_column2.append(j[1])

        global wb_marking_template
        sheet = wb_marking_template.get_sheet_by_name('Marking')

        # Clear all cells
        for i in range(2, 6):
            sheet.cell(row=i, column=2).value = ''
            
        now = datetime.now()
        dt_string = now.strftime("%d-%m-%Y-%H-%M-%S")
        sheet.cell(row=2, column=2).value = dt_string


        sheet.cell(row=9, column=1).value = 'PO NUMBER:'
        sheet.cell(row=9, column=2).value = self.lineEdit_6.text()

        global matrixHeading
        sheet.cell(row=3, column=2).value = matrixHeading[2]
        sheet.cell(row=4, column=2).value = matrixHeading[0]
        sheet.cell(row=5, column=2).value = matrixHeading[1]

        # Clear all cells
        for i in range(1, 3):
            for j in range(9, 31):
                sheet.cell(row=j, column=i).value = ''



        sheet.cell(row=9, column=1).value = 'PO NUMBER:'
        sheet.cell(row=9, column=2).value = PO_number

        for i in range(0, len(visible_columns)):

            sheet.cell(row=i + 10, column=1).value = excel_column1[i]
            sheet.cell(row=i + 10, column=2).value = excel_column2[i]

        # saveFileName = self.lineEdit.text() +' ' + self.lineEdit_6.text() + ' generated Marking ' + str(dt_string) + '.xlsx'
        # print(saveFileName)
        # self.label_5.setText(saveFileName + ' succesfully generated at root folder.')

        # We do not wish to save since we now generate all tabs in one file.
        #wb_marking_template.save(saveFileName)

        thin_border = Border(left=Side(style='thin'),
                             right=Side(style='thin'),
                             top=Side(style='thin'),
                             bottom=Side(style='thin'))

        for i in range(1,6):
            for j in range (9,31):
        # property cell.border should be used instead of cell.style.border
             sheet.cell(row=j, column=i).border = thin_border

    def export_to_excel_etsing(self):

        global visible_columns

        excel_column1 = []
        excel_column2 = []

        for i in visible_columns:
            j = i.split('//')

            if ':' not in j[0]:
                j[0] += ':'

            excel_column1.append(j[0])
            excel_column2.append(j[1])

        global wb_marking_template
        sheet = wb_marking_template.get_sheet_by_name('Etsing')

        # Clear all cells before writing to them to make sure nothing is in ram memory from earlier generate.
        for i in range(1, 3):
            for j in range(4, 23):
                # property cell.border should be used instead of cell.style.border
                sheet.cell(row=j, column=i).value = ''

        sheet.cell(row=1, column=1).value = 'Generated by' + ':'
        now = datetime.now()
        dt_string = now.strftime("%d-%m-%Y-%H-%M-%S")
        sheet.cell(row=2, column=1).value = 'Generated on ' + dt_string

        sheet.cell(row=4, column=1).value = 'PO NUMBER:'
        sheet.cell(row=4, column=2).value = self.lineEdit_6.text()
        sheet.cell(row=6, column=1).value = 'DATE PRODUCED:'
        sheet.cell(row=6, column=2).value = self.lineEdit_2.text()
        sheet.cell(row=5, column=1).value = 'SERIAL NUMBER:'
        sheet.cell(row=5, column=2).value = self.lineEdit.text()
        # sheet.cell(row=, column=1).value =


        row_add = 0
        col = 1
        for i in range(0, len(visible_columns)):
            sheet.cell(row=i + 7 + row_add, column=col).value = excel_column1[i]
            sheet.cell(row=i + 7 + row_add, column=col + 1).value = excel_column2[i]
            # if i == 2:
            #     col += 2
            #     row_add -= 5
            # elif i == 7:
            #     col += 2
            #     row_add -= 5
            #
            # elif i == 11:
            #     col += 2
            #     row_add -= 5

        # Save file name consists of serial number, po number, date, partnumber
        # saveFileName = self.lineEdit.text() +' ' + self.lineEdit_6.text()+ ' generated marking ' + str(dt_string) +'.xlsx'
        # print(saveFileName)
        # self.label_5.setText(saveFileName + ' succesfully generated at root folder.')

        thin_border = Border(left=Side(style='thin'),
                             right=Side(style='thin'),
                             top=Side(style='thin'),
                             bottom=Side(style='thin'))

        for i in range(1,3):
            for j in range (4,23):
        # property cell.border should be used instead of cell.style.border
             sheet.cell(row=j, column=i).border = thin_border



    # This function calls the above two, to make the complete document.
    def export_to_excel_skilt(self):

        try:
            self.export_to_excel_marking()
            self.export_to_excel_etsing()
            global visible_columns
            #print(visible_columns)

            excel_column1 = []
            excel_column2 = []

            for i in visible_columns:
                j = i.split('//')

                if ':' not in j[0]:
                    j[0] += ':'

                excel_column1.append(j[0])
                excel_column2.append(j[1])

            # If we suspect there are multiple probe lengths:
            for i in excel_column1:
                if 'PROBE' in i:
                    if len(excel_column2[excel_column1.index(i)]) > 7:
                        self.warning_message('Marking generated, but check the probe length please, is it possible that this matrix applies to multiple sensors with different probe lengths - considering updating and re-generating? ', 'Potential probe length issue')

            # If a value is just a placehlder, give user a warning.
            for i in excel_column2:
                if (i == ' NA') or (i == ' na') or (i == '') or (i == ' -') or (i == '-') or (i == ' '):
                    self.warning_message('Marking generated succesfully, but: ' + excel_column1[excel_column2.index(i)] + ' has no value, consider updating or removing this item and regenerating?', 'Incomplete item?' )


            global wb_marking_template
            sheet = wb_marking_template.get_sheet_by_name('Skilt')

            # Clear any residual info:
            # Loop through the cells which need to be empty at start to prevent bugs from previous generates.
            for i in range(1,11):
                for j in range (4,12):
                 sheet.cell(row=j, column=i).value = ''

            sheet.cell(row=1, column=1).value = 'Generated by' + ':'
            now = datetime.now()
            dt_string = now.strftime("%d-%m-%Y-%H-%M-%S")
            sheet.cell(row=2, column=1).value = 'Generated on ' + dt_string

            sheet.cell(row=5, column=1).value = 'PO NUMBER:'
            sheet.cell(row=4, column=1).value = 'SERIAL NUMBER:'
            sheet.cell(row=5, column=2).value = self.lineEdit_6.text()
            sheet.cell(row=9, column=1).value = 'DATE PRODUCED:'
            sheet.cell(row=9, column=2).value = self.lineEdit_2.text()
            sheet.cell(row=4, column=2).value = self.lineEdit.text()
            # sheet.cell(row=, column=1).value =



            row_add = 0
            col = 1
            for i in range(0, len(visible_columns)):
                sheet.cell(row=i + 6 + row_add, column=col).value = excel_column1[i]
                sheet.cell(row=i + 6 + row_add, column=col + 1).value = excel_column2[i]
                if i == 2:
                    col += 2
                    row_add -= 5
                elif i == 8:
                    col += 2
                    row_add -= 6

                elif i == 14:
                    col += 2
                    row_add -= 6

            # Save file name consists of type, partNumber, serial number, po number, date

            #Remove all illegal characters fromm part, sensor, ponum and serial num as these are used in save file name.
            part_number = ''
            sensor_type = ''
            PO_number = self.remove_illegal_characters(self.lineEdit_6.text())
            serial_number = self.remove_illegal_characters(self.lineEdit.text())

            for i in excel_column1:
                if 'pn' in i.lower():
                    part_number = self.remove_illegal_characters(excel_column2[excel_column1.index(i)])
                elif 'type' in i.lower():
                    sensor_type = self.remove_illegal_characters(excel_column2[excel_column1.index(i)])

            saveFileName = 'Marking generated for ' + sensor_type + ' ' + part_number + ' '+  serial_number +' ' + PO_number + str(dt_string) +'.xlsx'




            # Create a border around the skilt in excel
            thin_border = Border(left=Side(style='thin'),
                                 right=Side(style='thin'),
                                 top=Side(style='thin'),
                                 bottom=Side(style='thin'))
            # Loop through the cells which need a border. 
            for i in range(1,9):
                for j in range (4,10):
                 sheet.cell(row=j, column=i).border = thin_border

            global filename
            path_list = filename.split('/')
            # print(path_list)
            path_list.pop()

            path_string = ''
            for i in path_list:
                path_string += i
                path_string += ('\\')

            wb_marking_template.save(path_string + saveFileName)
            self.label_5.setText(saveFileName + ' succesfully generated at matrix folder.')


            pdfSaveFileName = saveFileName.replace('.xlsx', 'pdf')
            print(path_string + saveFileName)

            # SDP-6 needs an extra line to indicate which is high and which is low.
            if 'sdp-6' in pdfSaveFileName:
                sheet.cell(row=10, column=1).value = 'L'
                sheet.cell(row=10, column=6).value = 'H'

                # Create a border around the skilt in excel
                fat_border = Border(#left=Side(style='None'),
                                     #right=Side(style='None'),
                                     top=Side(style='thick'),
                                     bottom=Side(style='thick'))
                # Loop through the cells which need a border.
                for j in range(1, 12):
                    sheet.cell(row=10, column=j).border = fat_border


            if self.checkBox_3.isChecked():
                self.pdfGenerator(path_string + saveFileName, path_string +pdfSaveFileName , True)


        except Exception as e:
            print(e, ' // Excel generation failed')
            self.label_5.setText('e, // Excel generation failed, contact Ruben Speybrouck and send matrix file.')
            logging.exception('Failed to generate marking from: ', filename)
            logging.exception(visible_columns)

        wb_marking_template.close()

    # Make a PDF copy.
    def pdfGenerator(self, input_path, output_path,delete_bool):

        # Todo: fix the pdf generator bug that includes the path
        o = win32com.client.Dispatch("Excel.Application")
        o.Visible = False
        # before: wb_path = r'C:\Users\z0044wmy\Desktop\PYQTTESTS\\' + name1
        wb_path = input_path

        wb = o.Workbooks.Open(wb_path)
        ws_index_list = [1]  # say you want to print these sheets

        path_to_pdf = output_path
        print_area = 'A1:M100'

        for index in ws_index_list:
            # off-by-one so the user can start numbering the worksheets at 1
            ws = wb.Worksheets[index - 1]
            ws.PageSetup.Zoom = False
            # ws.PageSetup.FitToPagesTall = 1
            ws.PageSetup.FitToPagesWide = 1
            ws.PageSetup.PrintArea = print_area
        wb.WorkSheets(ws_index_list).Select()
        wb.ActiveSheet.ExportAsFixedFormat(0, path_to_pdf)

        wb.Close(True)  # save the workbook

        # Remove the input file to avoid it getting stuck.
        if delete_bool == True:
            path_remove_file = Path(input_path)
            path_remove_file.unlink()

    # Save layout


# A class for handling input windows, A lot of this is unnecessary but may be useful later.
class inputdialogdemo(QWidget):
    def __init__(self, parent=None):
        super(inputdialogdemo, self).__init__(parent)

        layout = QFormLayout()
        self.btn = QPushButton("Choose from list")
        self.btn.clicked.connect(self.getItem)

        self.le = QLineEdit()
        layout.addRow(self.btn, self.le)
        self.btn1 = QPushButton("get name")
        self.btn1.clicked.connect(self.gettext)

        self.le1 = QLineEdit()
        layout.addRow(self.btn1, self.le1)
        self.btn2 = QPushButton("Enter an integer")
        self.btn2.clicked.connect(self.getint)

        self.le2 = QLineEdit()
        layout.addRow(self.btn2, self.le2)
        self.setLayout(layout)
        self.setWindowTitle("Input Dialog demo")

    def getItem(self):

        try:
            # Get all the save files names from the CSV
            read_save_names = []
            with open('savefile.csv', newline='') as csv_file:
                csv_reader = csv.reader(csv_file, delimiter=',')
                for row in csv_reader:
                    # print(row[3])
                    read_save_names.append(row)
                #print(read_save_names)
                csv_file.close()



            items = []
            for i in read_save_names:
                items.append(i[0])

            item, ok = QInputDialog.getItem(self, "select layout to load",
                                            "list of saved layouts", items, 0, False)

            if ok and item:
                self.le.setText(item)
                return item

        # If the save file cannot be found, do nothing.
        except:
            print('save file not found')
            pass



    def gettext(self):

        try:
            text, ok = QInputDialog.getText(self, 'Enter new value', 'New value:')

            if ok:
                self.le1.setText(str(text))
                return text

        except:
            print('Error occured updating text value')
            pass



    def getint(self):
        num, ok = QInputDialog.getInt(self, "integer input dualog", "enter a number")

        if ok:
            self.le2.setText(str(num))



if __name__ == "__main__":


    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    app.setWindowIcon(QtGui.QIcon('tray2.ico'))
    MainWindow.setWindowIcon(QtGui.QIcon('tray2.ico'))

    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
