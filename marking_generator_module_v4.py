import openpyxl
import docx
import traceback
from datetime import datetime
from openpyxl.styles.borders import Border, Side

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QMainWindow, QApplication, QWidget, QAction, QTableWidget, QTableWidgetItem, QVBoxLayout, \
    QMessageBox, QDialog, QPushButton, QInputDialog, QLineEdit, QFormLayout, QFileDialog

import logging
logging.basicConfig(filename='app.log', filemode='a', format= '%(asctime)s - %(name)s - %(lineno)d - %(levelname)s - %(message)s')

import win32com.client
from pathlib import Path

from Mark_gen_git.GUIGENERATEDCODE import Ui_MainWindow

class UIMainWindowInherited(Ui_MainWindow):

    # We inherit from the generated PYQT GUI and override to add signal events.
    def setupUi(self, MainWindow):
        super().setupUi(MainWindow)
        self.pushButton.clicked.connect(self.open_dialog_box)
        self.listWidget.itemDoubleClicked.connect(self.column_hide)
        self.listWidget_2.itemDoubleClicked.connect(self.column_show)
        self.pushButton_5.clicked.connect(self.add__marking_item)
        self.pushButton_6.clicked.connect(self.move_item_up)
        self.pushButton_7.clicked.connect(self.move_item_down)
        self.pushButton_3.clicked.connect(self.export_to_excel_skilt)
        self.pushButton_2.clicked.connect(self.prev_part)
        self.pushButton_4.clicked.connect(self.next_part)
        self.pushButton_8.clicked.connect(self.edit_value)

        self.actionWEPS_OTHER.triggered.connect(lambda: self.manual_mode('WEPS'))
        self.actionSIMULATOR.triggered.connect(lambda: self.manual_mode('Simulator'))
        self.actionSDP_6.triggered.connect(lambda: self.manual_mode('SDP-6'))
        self.actionSDP_X.triggered.connect(lambda: self.manual_mode('SDPX'))

    # We inherit from the generated PYQT GUI and override to redefine, show, clear or hide gui items on boot.
    def retranslateUi(self, MainWindow):
        super().retranslateUi(MainWindow)

        self.listWidget.clear()
        self.listWidget_2.clear()

    # Fuction to edit values. Uses an instance of the inputdialoguedemoclass.
    def edit_value(self):

        try:
            item_to_edit = self.listWidget.currentItem().text()
            index_item_to_edit = self.visible_columns.index(item_to_edit)

            ex = inputdialogdemo()
            exInput = ex.gettext()
            #print(exInput)
            # Get the old label.
            old_text = self.visible_columns[index_item_to_edit].split('//')
            new_text = old_text[0] + ' // ' + exInput

            self.visible_columns[index_item_to_edit] = new_text

            self.update_columns()

        except:
            print('No value selected')

    # For generating marking without scanning a matrix.
    def manual_mode(self, type):

        #Clear the heading as we do not scan a document heading in this case.
        self.matrixHeading = []

        self.visible_columns = []

        self.hidden_columns = []

        # Set the self.filename to blank as no matrix was scanned. If not, previous scan would mess this up.
        self.filename = ''


        key_word_list = []
        if type == 'WEPS':
            key_word_list = self.keywords_regular

        elif type == 'SDPX':
            key_word_list = self.keywords_regular

        elif type == 'SDP-6':
            key_word_list = self.keywords_regular
        #todo add some changes here

        elif type == 'Simulator':
            key_word_list = self.keyWords_simulator

        for i in key_word_list:
            i_legal_characters_only = self.remove_all_none_alphanumeric_and_upper(i)
            print(i_legal_characters_only)

            # Seperate rules per sensor type
            if 'SDP' in type:

                if i_legal_characters_only == 'URL  FS':
                    i_legal_characters_only = 'MAX DP RANGE (URL):'

                elif i_legal_characters_only == 'CR SPAN':
                    i_legal_characters_only = 'DP CAL. PRESSURE RANGE:'

                elif i_legal_characters_only == 'PROCESS INTERFACE':
                    continue

                elif i_legal_characters_only == 'PROBE LENGTH':
                    continue

            elif type == 'WEPS':

                if i_legal_characters_only == 'CABLE DIRECTION':
                    continue

                elif i_legal_characters_only == 'URL  FS':
                    continue

                elif i_legal_characters_only == 'CR SPAN':
                    continue


            key_value = ''
            if 'TYPE' in i_legal_characters_only:
                key_value = type
            else:
                key_value = ''

            keyword_value_list = [i_legal_characters_only + ':', ' // ', key_value]
            self.visible_columns.append(str(keyword_value_list[0]) + str(keyword_value_list[1]) + str(keyword_value_list[2]))

        self.update_columns()

    # This function is used for matrices with multiple parts.
    def next_part(self):

        if self.table_coor_index< len(self.table_coordinates)-1:
            self.table_coor_index+= 1
            print('Moved to part ' + str(self.table_coor_index) + 'of '+ str(len(self.table_coordinates)))
            self.label_5.setText('Moved to part ' + str(self.table_coor_index) + 'of '+ str(len(self.table_coordinates)))
            self.find_keywords()

        else:
            self.label_5.setText('This is the last or only part in this matrix.')

    # This function is used for matrices with multiple parts.
    def prev_part(self):


        if self.table_coor_index> 1:
            self.table_coor_index-= 1
            self.label_5.setText('Moved to part ' + str(self.table_coor_index) + 'of ' + str(len(self.table_coordinates)))
            self.find_keywords()

        else:
            self.label_5.setText('This is the first or only part in this matrix.')

    # Open a dialogue box to find the matrix you wish to import.
    def open_dialog_box(self):

        try:
            file_name = QFileDialog.getOpenFileName()
            # Path is the first item in the list returned by above function, rest is debug info (I think)
            path = file_name[0]
            #print(path)
            #print(file_name)

            # Convert any 'doc' documents to 'docx' documents in the current directory.
            path_list = path.split('/')
            #print(path_list)
            path_list.pop()

            path_string = ''
            for i in path_list:
                path_string += i
                path_string += ('\\')

            #print(path_string)

            if '.doc' in path:
                if '.docx' not in path:
                    self.docx_converter(
                        comObject='Word.application',
                        dirPath=Path(path_string),
                        sourceExtension='doc',
                        destinationExtension='docx',
                        fileFormat=16,
                        dryRun=False
                    )
                    path += 'x'
                    #print(path)

            # run rest of program after converting doc to docx

            self.filename = path

            # This function finds all the keywords in the matrix and updates the two global column lists with them.
            self.find_keywords()

        except:
            print('no valid file selected')

    # Fuction to trigger warning messages
    def warning_message(self, text, title):
        msg = QMessageBox()
        msg.setText(text)
        msg.setIcon(QMessageBox.Information)
        msg.setWindowTitle("Possible issue with matrix")
        returnValue = msg.exec()
        if returnValue == QMessageBox.Ok:
            print('OK clicked')

    # Function to convert doc files into docx files. Used for SDP-6 matrices which typically are in .doc format which
    # python docx lib cannot read.
    def docx_converter(self, comObject, dirPath, sourceExtension, destinationExtension, fileFormat, dryRun=False):
        # This function converts all .doc files in the path folder to .docx files.
        msApp = win32com.client.Dispatch(comObject)

        for sourceFile in sorted(dirPath.rglob(f'*.{sourceExtension}')):

            destinationFile = sourceFile.with_suffix(f'.{destinationExtension}')

            if not destinationFile.is_file():

                print(f'Converting: {sourceFile}')

                if not dryRun:

                    try:

                        if comObject == 'Excel.application':

                            doc = msApp.Workbooks.Open(str(sourceFile))

                        elif comObject == 'Word.application':

                            doc = msApp.Documents.Open(str(sourceFile))

                        doc.SaveAs(str(destinationFile), FileFormat=fileFormat)

                        doc.Close()

                    except Exception as e:

                        print(f'Failed to Convert: {sourceFile} : {e}')

        msApp.Quit()

        return

    # Function used to separate a file with multiple parts in sections stored as begin and end coordinates for cells.
    def find_coordinates_instrument_datasheet(self):

  
        self.table_coordinates = []

        self.label_5.setText('File: ' + self.filename + ' succesfully imported, scanned marking keywords.')
        doc = docx.Document(self.filename)
        tables = doc.tables

        allCells = []
        found_words = []

        for table in tables:
            for row in table.rows:
                for cell in row.cells:
                    allCells.append(cell.text)
                    # print(row.cells[])

        # print(allCells)

        # Locate the coordinates of each table so we can cycle through tables.
        last_coor = 0
        new_coor = 0

        # Only read after the last pages of the datasheet. Usuallly surface protection is a unique item at the end
        # of the datasheet so we use this to trigger that we are now at the end and ready to start reading.
        start_read = False
        for i in range(0,len(allCells)):
            if 'Surface' in allCells[i]:
                start_read = True

            if start_read == True:

                # Supplier is the first word in the marking table and occurs 6 times because it is a merged cell.
                if 'Supplier' in allCells[i]:
                    #print(allCells[i])
                    new_coor = i

                    # If 10 table cells have passed since the last time we detected the word 'supplier', assume that
                    # next time we encounter it, it is a new start of a table.

                    if last_coor +10 < new_coor:
                        coordinates_to_append = [last_coor, new_coor]
                        self.table_coordinates.append(coordinates_to_append)
                        last_coor = new_coor

        # The last item does not have a next item to fence it off, so we add it manually.
        self.table_coordinates.append([last_coor, last_coor+50])
        #print(self.table_coordinates)

    # Function to find keywords based on coordinates and return them to be used in other functions to update GUI.
    def find_keywords_instrument_datasheet(self, coordinates):

        #Todo incorporate legal character search.
        keyWords_legal_characters_only = []
        for i in self.datasheet_keyWords:
            i_legal = self.remove_all_none_alphanumeric_and_upper(i)
            if i_legal not in keyWords_legal_characters_only:
                keyWords_legal_characters_only.append(i_legal)

        keyWords_singelecol_legal_characters_only = []
        for i in self.keyWordsSingleCol_datasheet:
            i_legal = self.remove_all_none_alphanumeric_and_upper(i)
            if i_legal not in keyWords_singelecol_legal_characters_only:
                keyWords_singelecol_legal_characters_only.append(i_legal)


        self.label_5.setText('File: ' + self.filename + ' Reading partnumber x from instrument datasheet.')
        logging.info('succesfully loaded: ', self.filename )
        doc = docx.Document(self.filename)
        tables = doc.tables

        allCells = []
        found_words = []

        for table in tables:
            for row in table.rows:
                for cell in row.cells:
                    allCells.append(cell.text)
                    # print(cell.text)

        for i in range (coordinates[0], coordinates[1]):
            unique_value = True
            if allCells[i] in self.datasheet_keyWords:
                #print('FOUND:', allCells[i])
                # If not first part of a merged cell
                if allCells[i] != allCells[i+1]:
                    # This is wrong in the table so we correct
                    if allCells[i] == 'Serial No.:':
                        keyword_value_tuple = ('Sensor type', ' // ', allCells[i + 1])
                    else:
                        keyword_value_tuple = (allCells[i], ' // ', allCells[i + 1])

                    found_words.append(keyword_value_tuple)

        #print(found_words)
        return found_words

    # This function is triggered by the open_dialogue_box and finds the actual keywords.
    def find_keywords(self):
        self.visible_columns = []
        self.hidden_columns = []

        # We need to set the cordinates. If we do not we risk that a regular matrix could have bugs after first loading a tfmc pure datasheet.
        # Get information from the word file header
        doc = docx.Document(self.filename)
        headerCells = []
        section = doc.sections[0]
        header = section.header
        for table in header.tables:
            for row in table.rows:
                for cell in row.cells:
                    if (cell.text != '') and (cell.text != ':'):
                        headerCells.append(cell.text)
                    # print(row.cells[])
        #print(headerCells)

        self.matrixHeading = []
        try:
            self.matrixHeading.append(headerCells[1])
            self.matrixHeading.append(headerCells[5])
            self.matrixHeading.append(headerCells[7])

        except:
            print('Header cells could not be read properly')
        # This removes any newline characters that may be in the doc number


        new_text = ''
        for i in self.matrixHeading[0]:
            if i.isnumeric:
                new_text += i
        #print(new_text)
        new_text.replace('\t', '')
        self.matrixHeading.pop(0)
        self.matrixHeading.append(new_text)
        #print(self.matrixHeading[0])
        #print(self.matrixHeading)


        try:

            # if not a datasheet type template containing many markings
            found_words = []

            if 'DATASHEET TFMC' not in self.filename:

                self.label_5.setText('File: ' + self.filename + ' succesfully imported, scanned marking keywords.')
                doc = docx.Document(self.filename)
                tables = doc.tables

                # Set the keywords based on the type of file that was loaded
                keyWords = self.keywords_regular
                # These keywords are always in the header of a Matrix.
                keyWordsSingleCol = self.keywords_regular_SingleCol

                if 'simulator' in self.filename.lower():
                    keyWords = self.keyWords_simulator
                    keyWordsSingleCol = self.keyWords_simulator_SingleCol


                keyWords_legal_characters_only = []
                for i in keyWords:
                    i_legal = self.remove_all_none_alphanumeric_and_upper(i)
                    if i_legal not in keyWords_legal_characters_only:
                        keyWords_legal_characters_only.append(i_legal)

                keyWords_singelecol_legal_characters_only = []
                for i in keyWordsSingleCol:
                    i_legal = self.remove_all_none_alphanumeric_and_upper(i)
                    if i_legal not in keyWords_singelecol_legal_characters_only:
                        keyWords_singelecol_legal_characters_only.append(i_legal)

                # Load every single table cell of the matrix in a very long list.
                allCells = []
                for table in tables:
                    for row in table.rows:
                        for cell in row.cells:
                            allCells.append(cell.text)

                            # print(row.cells[])
                print(allCells)

                # Loop through every single Matrix cell and check: a) is this a keyword i need, b) do I already have it?
                for i in allCells:
                    #print(i)
                    #print('-------------------')

                    unique_value = True

                    # Surface is usually the last table of the matrix, if this is reached, stop reading.
                    # The purpose is to prevent reading keywords at the bottom of the file which may have similar keywords.
                    # if 'Surface' in i:
                    #     break

                    # Strip the keyword if all none-letter or numeric values.
                    i_legal_characters_only = self.remove_all_none_alphanumeric_and_upper(i)
                    print('old word: ', i)
                    print('word:',i_legal_characters_only)

                    if i_legal_characters_only in keyWords_legal_characters_only:

                        # Check if we already found a value. We only want the first label and value, no copies.
                        # Also check if perhaps a version exists with or without colon.
                        print('found:   ' ,i_legal_characters_only)
                        for j in found_words:

                            #print(i_legal_characters_only)
                            # Found words is a list of tuples consisting of three items. J[0] is the keyword followed by
                            # '//' and the value. Here we check if the keyword we find has been found before,
                            # Possibly with the colon at a different location.
                            if i_legal_characters_only +':' == j[0]:
                                unique_value = False
                            # elif i.replace(':', '') == j[0]:
                            #     unique_value = False
                            # elif i + ':' == j[0]:
                            #     unique_value = False

                        # If the keyword had not been found earlier in the document, take it's index.
                        if unique_value == True:
                            #print('FOUND: ',i)
                            # If the keyword is part of the table on page 1, the value is at the +1 index as this
                            # tabel only has 2 columns.

                            index = allCells.index(i)+1

                            # If the item is not part of the 2 column table on the first page of the doc,
                            # Then the keyword is on colomn 1, while the value is always in column 3, so add 2 to index.
                            if i_legal_characters_only not in keyWords_singelecol_legal_characters_only:
                                index = allCells.index(i) + 2
                                # Some word files have a weird bug where index + 1 and index + 2 exist twice even
                                # though the word document only visualises once. When this happens, go to +3.
                                if allCells[index] == allCells[index-1]:
                                    index = allCells.index(i) + 3

                            keyword_value_tuple = (i_legal_characters_only +':', ' // ', allCells[index])

                            # A separate set of rules for SDP-5/ SDP-8 as the keyword is ugly in the matrix.

                            try:
                                #print(found_words[3][2])
                                if ('SDP' in found_words[3][2]):
                                    if i == 'URL / FS':
                                        keyword_value_tuple = ('MAX DP RANGE (URL):', ' // ', allCells[index])
                                    elif i == 'CR / Span':
                                        keyword_value_tuple = ('DP CAL. PRESSURE RANGE:', ' // ', allCells[index])

                                    # skip to the next for loop item as SPD-5 process interface is not part of SDP-5 marking
                                    elif i == 'Process interface':
                                        continue

                            except:
                                print('Product type unknown')
                                tb = traceback.format_exc()
                                print(tb)

                            # A separate set of rules for WEPS to remove some keywords not commonly used for WEPS.
                            try:
                                #print(found_words[3][2])
                                if ('SDP' not in found_words[3][2]):
                                    print('This is a WEPS SENSOR, applying WEPS formatting')
                                    if i_legal_characters_only == 'CABLE DIRECTION':
                                        continue
                                    elif i_legal_characters_only == 'ELECTRICAL OUTPUT COMMUNICATION PROTOCOL':
                                        continue

                            except:
                                print('Product type unknown')



                            # Found words is a list of all the keywords we found + '//' + corresponding value.
                            found_words.append(keyword_value_tuple)
                #print(found_words)

            elif 'DATASHEET TFMC' in self.filename:
                print('file was a datasheet')
                self.label_5.setText('Instrument data sheet loaded.')
                self.find_coordinates_instrument_datasheet()
                found_words = self.find_keywords_instrument_datasheet(self.table_coordinates[self.table_coor_index])

            self.listWidget.clear()

            #print(found_words)
            self.visible_columns = []
            for i in found_words:
                # convert the tuples in to a list of strings.
                self.visible_columns.append(str(i[0] + i[1] + i[2]))

            #print(self.visible_columns)

            self.update_columns()

        except Exception as e:
            print(e, ' // no valid file selected')
            logging.exception('exception occured -', self.filename, ' - failed to read')
            self.label_5.setText('e, // No compatible word specification matrix file was loaded, only docx format supported.')

    # ------------------ Operations on found keywords

    # Move a keyword value pair to the hidden columnn widget
    def column_hide(self):

        try:
            list_item = self.listWidget.currentItem().text()
            print(list_item)

            self.visible_columns.remove(list_item)
            self.hidden_columns.append(list_item)

        except AttributeError:
            print('hide; no item selected')

        self.update_columns()

    # Move a keyword value pair to the visible columnn widget
    def column_show(self):

        try:
            list_item = self.listWidget_2.currentItem().text()
            print(list_item)

            self.visible_columns.append(list_item)
            self.hidden_columns.remove(list_item)


        except AttributeError:
            print('Show, no item selected')

        self.update_columns()

    # This function updates the GUI - used when hide/show or find keyword operations are done.
    def update_columns(self):
        self.listWidget.clear()
        self.listWidget_2.clear()

        self.listWidget.addItems(self.visible_columns)
        self.listWidget_2.addItems(self.hidden_columns)

    # Add a new label and value pair
    def add__marking_item(self):
        item_to_add = []
        item_to_add.append(self.lineEdit_4.text())
        item_to_add.append(' // ')
        item_to_add.append(self.lineEdit_5.text())

        self.visible_columns.append(str(item_to_add[0] + item_to_add[1] + item_to_add[2]))
        self.update_columns()

    # Move item up in the visible colomn list in the GUI.
    def move_item_up(self):

        try:
            item_to_move = self.listWidget.currentItem().text()
            index_item_to_move = self.visible_columns.index(item_to_move)

            if index_item_to_move > 0:
                new_index_item_to_move = index_item_to_move - 1
                self.visible_columns.pop(index_item_to_move)
                self.visible_columns.insert(new_index_item_to_move, item_to_move)
                self.update_columns()
                #self.listWidget.setCurrentItem(self.visible_columns[2])
                self.listWidget.setCurrentRow(new_index_item_to_move)

            else:
                print('operation move failed because item on top or bottom')

        except AttributeError:
            print('no item selected')

    # Move item down in the visible colomn list in the GUI.
    def move_item_down(self):

        try:
            item_to_move = self.listWidget.currentItem().text()
            index_item_to_move = self.visible_columns.index(item_to_move)


            new_index_item_to_move = index_item_to_move + 1
            self.visible_columns.pop(index_item_to_move)
            self.visible_columns.insert(new_index_item_to_move, item_to_move)
            self.update_columns()
            self.listWidget.setCurrentRow(new_index_item_to_move)


        except AttributeError:
            print('no item selected or item already at bottom')

    # purge all illegal characters.
    def remove_illegal_characters(self, string):
        legal_string = ''

        for i in string:

            if i.isalpha():
                legal_string += i.lower()

            elif i.isnumeric():
                legal_string += i.lower()

            else:
                legal_string += '-'

        return legal_string

    # purge even more illegal characters.
    def remove_all_none_alphanumeric_and_upper(self, string):
        # Todo remove spaces at beginning and end
        legal_string = ''
        loop_counter = 0

        for i in string:

            if i.isalpha():
                legal_string += i.upper()

            elif i.isnumeric():
                legal_string += i.upper()

            # Use only spaces between words. Not spaces in beginning or end of the cell content.
            elif i.isspace():
                if (loop_counter > 2) and (loop_counter < (len(string) -2)):
                    legal_string += i.upper()
            loop_counter += 1
        return legal_string

    # This function opens the excel workbook template, generates to skilt sheet and calls the other generate functions - should have been two functions really.
    def export_to_excel_skilt(self):

        # If no matrix was scanned and you are creating a manual marking
        if self.filename == '':
            self.filename = str(Path.cwd()).replace('\\', '/') + '/markingTemplate2.xlsx'

        try:

            self.export_to_excel_marking()
            self.export_to_excel_etsing()

            # print(self.visible_columns)

            excel_column1 = []
            excel_column2 = []

            for i in self.visible_columns:
                j = i.split('//')

                if ':' not in j[0]:
                    j[0] += ':'

                excel_column1.append(j[0])
                excel_column2.append(j[1])

            # If we suspect there are multiple probe lengths:
            for i in excel_column1:
                if 'PROBE' in i:
                    if len(excel_column2[excel_column1.index(i)]) > 7:
                        self.warning_message(
                            'Marking generated, but check the probe length please, is it possible that this matrix applies to multiple sensors with different probe lengths - considering updating and re-generating? ',
                            'Potential probe length issue')

            # If a value is just a placehlder, give user a warning.
            for i in excel_column2:
                if (i == ' NA') or (i == ' na') or (i == '') or (i == ' -') or (i == '-') or (i == ' '):
                    self.warning_message('Marking generated succesfully, but: ' + excel_column1[excel_column2.index(
                        i)] + ' has no value, consider updating or removing this item and regenerating?',
                                         'Incomplete item?')
                # If there is reference to this being a simulator
                if 'simulator' in i.lower():
                    self.export_simulator()

            sheet = self.wb_marking_template.get_sheet_by_name('Skilt')

            # Clear any residual info:
            # Loop through the cells which need to be empty at start to prevent bugs from previous generates.
            for i in range(1, 11):
                for j in range(4, 10):
                    sheet.cell(row=j, column=i).value = ''

            sheet.cell(row=1, column=1).value = 'Generated by' + ':'
            now = datetime.now()
            dt_string = now.strftime("%d-%m-%Y-%H-%M-%S")
            sheet.cell(row=2, column=1).value = 'Generated on ' + dt_string

            sheet.cell(row=5, column=1).value = 'PO NUMBER:'
            sheet.cell(row=4, column=1).value = 'SERIAL NUMBER:'
            sheet.cell(row=5, column=2).value = self.lineEdit_6.text()
            sheet.cell(row=9, column=1).value = 'DATE PRODUCED:'
            sheet.cell(row=9, column=2).value = self.lineEdit_2.text()
            sheet.cell(row=4, column=2).value = self.lineEdit.text()
            # sheet.cell(row=, column=1).value =

            row_add = 0
            col = 1
            for i in range(0, len(self.visible_columns)):
                sheet.cell(row=i + 6 + row_add, column=col).value = excel_column1[i]
                sheet.cell(row=i + 6 + row_add, column=col + 1).value = excel_column2[i]
                if i == 2:
                    col += 2
                    row_add -= 5
                elif i == 8:
                    col += 2
                    row_add -= 6

                elif i == 14:
                    col += 2
                    row_add -= 6

            # Save file name consists of type, partNumber, serial number, po number, date

            # Remove all illegal characters fromm part, sensor, ponum and serial num as these are used in save file name.
            part_number = ''
            sensor_type = ''
            PO_number = self.remove_illegal_characters(self.lineEdit_6.text())
            serial_number = self.remove_illegal_characters(self.lineEdit.text())

            for i in excel_column1:
                if 'pn' in i.lower():
                    part_number = self.remove_illegal_characters(excel_column2[excel_column1.index(i)])
                elif 'type' in i.lower():
                    sensor_type = self.remove_illegal_characters(excel_column2[excel_column1.index(i)])

            savefilename = 'Marking generated for ' + sensor_type + ' ' + part_number + ' ' + serial_number + ' ' + PO_number + str(
                dt_string) + '.xlsx'

            # Create a border around the skilt in excel
            thin_border = Border(left=Side(style='thin'),
                                 right=Side(style='thin'),
                                 top=Side(style='thin'),
                                 bottom=Side(style='thin'))
            # Loop through the cells which need a border.
            for i in range(1, 9):
                for j in range(4, 10):
                    sheet.cell(row=j, column=i).border = thin_border

            path_list = self.filename.split('/')
            # print(path_list)
            path_list.pop()

            path_string = ''
            for i in path_list:
                path_string += i
                path_string += ('\\')

            self.wb_marking_template.save(path_string + savefilename)
            self.label_5.setText(savefilename + ' , succesfully generated at: ' + self.filename)

            pdfSavefilename = savefilename.replace('.xlsx', 'pdf')
            print(path_string + savefilename)

            # # SDP-6 needs an extra line to indicate which is high and which is low.
            # if ('sdp-6' in pdfSaveself.filename) or ('SDP-6' in pdfSaveself.filename):
            #     sheet.cell(row=10, column=1).value = 'L'
            #     sheet.cell(row=10, column=6).value = 'H'
            #
            #     # Create a border around the skilt in excel
            #     fat_border = Border(#left=Side(style='None'),
            #                          #right=Side(style='None'),
            #                          top=Side(style='thick'),
            #                          bottom=Side(style='thick'))
            #     # Loop through the cells which need a border.
            #     for j in range(1, 12):
            #         sheet.cell(row=10, column=j).border = fat_border

            if self.checkBox_3.isChecked():
                self.pdfGenerator(path_string + savefilename, path_string + pdfSavefilename, True)


        except Exception as e:
            print(e, ' // Excel generation failed')
            self.label_5.setText('e, // Excel generation failed, contact Ruben Speybrouck and send matrix file.')
            logging.exception('Failed to generate marking from: ', self.filename)
            logging.exception(self.visible_columns)

        self.wb_marking_template.close()

    # Export to the marking sheet
    def export_to_excel_marking(self):

        excel_column1 = []
        excel_column2 = []
        PO_number = self.remove_illegal_characters(self.lineEdit_6.text())

        for i in self.visible_columns:
            j = i.split('//')

            # The layout in spec matrix sometimes lacks ':' which looks shitty
            if ':' not in j[0]:
                j[0] += ':'

            excel_column1.append(j[0])
            excel_column2.append(j[1])

        sheet = self.wb_marking_template.get_sheet_by_name('Marking')

        # Clear all cells
        for i in range(2, 6):
            sheet.cell(row=i, column=2).value = ''
            
        now = datetime.now()
        dt_string = now.strftime("%d-%m-%Y-%H-%M-%S")
        sheet.cell(row=2, column=2).value = dt_string


        sheet.cell(row=9, column=1).value = 'PO NUMBER:'
        sheet.cell(row=9, column=2).value = self.lineEdit_6.text()


        if len(self.matrixHeading) == 0:
            self.matrixHeading.append('Specification matrix reference not found, edit manually')
            self.matrixHeading.append('Specification matrix reference not found, edit manually')
            self.matrixHeading.append('Specification matrix reference not found, edit manually')


        sheet.cell(row=3, column=2).value = self.matrixHeading[2]
        sheet.cell(row=4, column=2).value = self.matrixHeading[0]
        sheet.cell(row=5, column=2).value = self.matrixHeading[1]

        # Clear all cells
        for i in range(1, 3):
            for j in range(9, 31):
                sheet.cell(row=j, column=i).value = ''



        sheet.cell(row=9, column=1).value = 'PO NUMBER:'
        sheet.cell(row=9, column=2).value = PO_number

        for i in range(0, len(self.visible_columns)):

            sheet.cell(row=i + 10, column=1).value = excel_column1[i]
            sheet.cell(row=i + 10, column=2).value = excel_column2[i]

        # saveself.filename = self.lineEdit.text() +' ' + self.lineEdit_6.text() + ' generated Marking ' + str(dt_string) + '.xlsx'
        # print(saveself.filename)
        # self.label_5.setText(saveself.filename + ' succesfully generated at root folder.')

        # We do not wish to save since we now generate all tabs in one file.
        #self.wb_marking_template.save(saveself.filename)

        thin_border = Border(left=Side(style='thin'),
                             right=Side(style='thin'),
                             top=Side(style='thin'),
                             bottom=Side(style='thin'))

        for i in range(1,6):
            for j in range (9,31):
        # property cell.border should be used instead of cell.style.border
             sheet.cell(row=j, column=i).border = thin_border

    # Export to the etsing sheet
    def export_to_excel_etsing(self):

        excel_column1 = []
        excel_column2 = []

        for i in self.visible_columns:
            j = i.split('//')

            if ':' not in j[0]:
                j[0] += ':'

            excel_column1.append(j[0])
            excel_column2.append(j[1])

        sheet = self.wb_marking_template.get_sheet_by_name('Etsing')

        # Clear all cells before writing to them to make sure nothing is in ram memory from earlier generate.
        for i in range(1, 3):
            for j in range(4, 23):
                # property cell.border should be used instead of cell.style.border
                sheet.cell(row=j, column=i).value = ''

        sheet.cell(row=1, column=1).value = 'Generated by' + ':'
        now = datetime.now()
        dt_string = now.strftime("%d-%m-%Y-%H-%M-%S")
        sheet.cell(row=2, column=1).value = 'Generated on ' + dt_string

        sheet.cell(row=4, column=1).value = 'PO NUMBER:'
        sheet.cell(row=4, column=2).value = self.lineEdit_6.text()
        sheet.cell(row=6, column=1).value = 'DATE PRODUCED:'
        sheet.cell(row=6, column=2).value = self.lineEdit_2.text()
        sheet.cell(row=5, column=1).value = 'SERIAL NUMBER:'
        sheet.cell(row=5, column=2).value = self.lineEdit.text()
        # sheet.cell(row=, column=1).value =


        row_add = 0
        col = 1
        for i in range(0, len(self.visible_columns)):
            sheet.cell(row=i + 7 + row_add, column=col).value = excel_column1[i]
            sheet.cell(row=i + 7 + row_add, column=col + 1).value = excel_column2[i]
            # if i == 2:
            #     col += 2
            #     row_add -= 5
            # elif i == 7:
            #     col += 2
            #     row_add -= 5
            #
            # elif i == 11:
            #     col += 2
            #     row_add -= 5

        # Save file name consists of serial number, po number, date, partnumber
        # saveself.filename = self.lineEdit.text() +' ' + self.lineEdit_6.text()+ ' generated marking ' + str(dt_string) +'.xlsx'
        # print(saveself.filename)
        # self.label_5.setText(saveself.filename + ' succesfully generated at root folder.')

        thin_border = Border(left=Side(style='thin'),
                             right=Side(style='thin'),
                             top=Side(style='thin'),
                             bottom=Side(style='thin'))

        for i in range(1,3):
            for j in range (4,23):
        # property cell.border should be used instead of cell.style.border
             sheet.cell(row=j, column=i).border = thin_border

    # Export to the simulator sheet
    def export_simulator(self):
        try:
                # print(self.visible_columns)

                excel_column1 = []
                excel_column2 = []

                for i in self.visible_columns:
                    j = i.split('//')

                    if ':' not in j[0]:
                        j[0] += ':'

                    excel_column1.append(j[0])
                    excel_column2.append(j[1])

                # If we suspect there are multiple probe lengths:

                sheet = self.wb_marking_template.get_sheet_by_name('Simulator')

                # Clear any residual info:
                # Loop through the cells which need to be empty at start to prevent bugs from previous generates.
                for i in range(3, 4):
                    for j in range(1, 30):
                        sheet.cell(row=j, column=i).value = ''

                # Save file name consists of type, partNumber, serial number, po number, date

                # Remove all illegal characters fromm part, sensor, ponum and serial num as these are used in save file name.
                part_number = ''
                sensor_type = ''
                PO_number = self.remove_illegal_characters(self.lineEdit_6.text())
                serial_number = self.remove_illegal_characters(self.lineEdit.text())

                cal_pressure_range = ''
                fixed_value_pressure = ''
                calibrated_temperature_range = ''
                fixed_value_temperature = ''
                SW_core = ''
                baud_rate = ''
                heart_beat = ''
                SW_application = ''

                for i in excel_column1:
                    if 'pn' in i.lower():
                        part_number = excel_column2[excel_column1.index(i)]
                    elif 'type' in i.lower():
                        sensor_type = excel_column2[excel_column1.index(i)]
                    elif 'pressure range' in i.lower():
                        cal_pressure_range = excel_column2[excel_column1.index(i)]
                    elif 'value pressure' in i.lower():
                        fixed_value_pressure = excel_column2[excel_column1.index(i)]
                    elif 'temperature range' in i.lower():
                        calibrated_temperature_range = excel_column2[excel_column1.index(i)]
                    elif 'value temperature' in i.lower():
                        fixed_value_temperature = excel_column2[excel_column1.index(i)]
                    elif 'baud' in i.lower():
                        baud_rate = excel_column2[excel_column1.index(i)]
                    elif 'heart' in i.lower():
                        heart_beat = excel_column2[excel_column1.index(i)]
                    elif 'core' in i.lower():
                        SW_core = excel_column2[excel_column1.index(i)]
                    elif 'application' in i.lower():
                        SW_application = excel_column2[excel_column1.index(i)]

                sheet.cell(row=1, column=3).value = sensor_type
                sheet.cell(row=2, column=3).value = serial_number
                sheet.cell(row=3, column=3).value = part_number
                sheet.cell(row=4, column=3).value = PO_number
                sheet.cell(row=5, column=3).value = 'SW: Core ' + SW_core + ' /Application ' + SW_application
                sheet.cell(row=6, column=3).value = baud_rate
                sheet.cell(row=7, column=3).value = heart_beat
                sheet.cell(row=8, column=3).value = ' -20 to +50°C'
                sheet.cell(row=9, column=3).value = 'IP 67 (When installed in Pelicase)'

                sheet.cell(row=13, column=1).value = sensor_type
                sheet.cell(row=14, column=1).value = calibrated_temperature_range + ' / ' + cal_pressure_range
                sheet.cell(row=15, column=1).value = 'FV:' + fixed_value_temperature + ' / ' + fixed_value_pressure + ' / NODE ID: '


                # Create a border around the skilt in excel
                thin_border = Border(left=Side(style='thin'),
                                     right=Side(style='thin'),
                                     top=Side(style='thin'),
                                     bottom=Side(style='thin'))
                # Loop through the cells which need a border.
                for i in range(2, 4):
                    for j in range(1, 10):
                        sheet.cell(row=j, column=i).border = thin_border

                for i in range(13,16):
                    sheet.cell(row=i, column=1).border = thin_border

        except:
            print('failed to generate simulator marking tab')

    # Make a PDF copy of the marking export - what actually happens is that we generate the entire excel file, print the first page, print to pdf and delete the pdf.
    def pdfGenerator(self, input_path, output_path,delete_bool):

        # Todo: fix the pdf generator bug that includes the path
        o = win32com.client.Dispatch("Excel.Application")
        o.Visible = False
        # before: wb_path = r'C:\Users\z0044wmy\Desktop\PYQTTESTS\\' + name1
        wb_path = input_path

        wb = o.Workbooks.Open(wb_path)
        ws_index_list = [1]  # say you want to print these sheets

        path_to_pdf = output_path
        print_area = 'A1:M100'

        for index in ws_index_list:
            # off-by-one so the user can start numbering the worksheets at 1
            ws = wb.Worksheets[index - 1]
            ws.PageSetup.Zoom = False
            # ws.PageSetup.FitToPagesTall = 1
            ws.PageSetup.FitToPagesWide = 1
            ws.PageSetup.PrintArea = print_area
        wb.WorkSheets(ws_index_list).Select()
        wb.ActiveSheet.ExportAsFixedFormat(0, path_to_pdf)

        wb.Close(True)  # save the workbook

        # Remove the input file to avoid it getting stuck.
        if delete_bool == True:
            path_remove_file = Path(input_path)
            path_remove_file.unlink()


# A class for handling input windows, A lot of this is unnecessary but may be useful later. This is used for the EDIT button.
class inputdialogdemo(QWidget):
    def __init__(self, parent=None):
        super(inputdialogdemo, self).__init__(parent)

        layout = QFormLayout()
        self.btn = QPushButton("Choose from list")
        self.btn.clicked.connect(self.getItem)

        self.le = QLineEdit()
        layout.addRow(self.btn, self.le)
        self.btn1 = QPushButton("get name")
        self.btn1.clicked.connect(self.gettext)

        self.le1 = QLineEdit()
        layout.addRow(self.btn1, self.le1)
        self.btn2 = QPushButton("Enter an integer")
        self.btn2.clicked.connect(self.getint)

        self.le2 = QLineEdit()
        layout.addRow(self.btn2, self.le2)
        self.setLayout(layout)
        self.setWindowTitle("Input Dialog demo")

    def getItem(self):

        try:
            # Get all the save files names from the CSV
            read_save_names = []
            with open('savefile.csv', newline='') as csv_file:
                csv_reader = csv.reader(csv_file, delimiter=',')
                for row in csv_reader:
                    # print(row[3])
                    read_save_names.append(row)
                #print(read_save_names)
                csv_file.close()



            items = []
            for i in read_save_names:
                items.append(i[0])

            item, ok = QInputDialog.getItem(self, "select layout to load",
                                            "list of saved layouts", items, 0, False)

            if ok and item:
                self.le.setText(item)
                return item

        # If the save file cannot be found, do nothing.
        except:
            print('save file not found')
            pass



    def gettext(self):

        try:
            text, ok = QInputDialog.getText(self, 'Enter new value', 'New value:')

            if ok:
                self.le1.setText(str(text))
                return text

        except:
            print('Error occured updating text value')
            pass



    def getint(self):
        num, ok = QInputDialog.getInt(self, "integer input dualog", "enter a number")

        if ok:
            self.le2.setText(str(num))


if __name__ == "__main__":


    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    app.setWindowIcon(QtGui.QIcon('tray2.ico'))
    MainWindow.setWindowIcon(QtGui.QIcon('tray2.ico'))

    # Here we refer to the inherited sub class rather than the super class, IMPORTANT!!!
    ui = UIMainWindowInherited()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
