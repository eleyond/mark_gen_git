import openpyxl
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QMainWindow, QApplication, QWidget, QAction, QTableWidget, QTableWidgetItem, QVBoxLayout, \
    QMessageBox, QDialog, QPushButton, QInputDialog, QLineEdit, QFormLayout, QFileDialog



class Ui_MainWindow(object):

    def __init__(self):
        # Instance variables related to file or gui state

        # This stores the coordinates of the current scanned matrix (TFMC only)
        self.table_coordinates = []

        # This stores the matrix table currently being viewed (TFMC only)
        self.table_coor_index = 1

        # This stores information from the heading of the current matrix file
        self.matrixHeading = []

        # These class instance variables hold the content for to view in the listwidget on screen and to import in the excel marking.
        self.visible_columns = []

        self.hidden_columns = []

        # Store the name and path of the file you wish to load. Used both to determine where to write new files as to determine where to read.
        self.filename = ''

        self.wb_marking_template = openpyxl.load_workbook('markingTemplate2.xlsx')

        self.keywords_regular = ['Siemens Type product:',
                                 'Client Name:',
                                 'Customer PN',
                                 'Siemens Drawing nr.',
                                 'CR / Span',
                                 'URL / FS',
                                 'Probe length',
                                 'Max Test Pressure',
                                 'Calibrated Pressure Range',
                                 'Pressure Calibration Temperature Range',
                                 'Pressure Calibrated Temperature Range',
                                 'Calibrated Temperature Range',
                                 'Mat Spec',
                                 'API 6A Mat/Temp Class',
                                 'Process interface',
                                 'Protocol Address',
                                 'Cable direction',
                                 'Test Pressure',
                                 'PROBE LENGTH',
                                 ]

        # These keywords are always in the header of a Matrix.
        self.keywords_regular_SingleCol = ['Siemens Type product:',
                                           'Client Name:',
                                           'Customer PN',
                                           'Siemens Drawing nr.', ]

        self.keyWords_simulator = ['Siemens Type product:',
                                   'Client Name:',
                                   'Customer PN',
                                   'Calibrated pressure range',
                                   'calibrated temperature range',
                                   'Fixed value Pressure',
                                   'fixed value temperature',
                                   'baud rate',
                                   'Heart beat (CANopen only) ',
                                   'core',
                                   'application',
                                   'Siemens Drawing nr.',

                                   ]

        # These keywords are always in the header of a Matrix.
        self.keyWords_simulator_SingleCol = ['Siemens Type product:',
                                             'Customer PN',
                                             'Siemens Drawing nr.',
                                             'Client Name:', ]

        self.datasheet_keyWords = [
            'Serial No.:',
            'Customer PN :',
            'Customer PN:',
            'GA Drwg :',
            'GA Drwg:',
            'Sensor Type',
            'Probe  length :',
            'Probe  length:',
            'Cal/Work. Pressure range:',
            'Cal. Temp. range:',
            'Max test pressure:',
            'Mat. Class/Temp:',
            'Mat. Class/Temp: ',
            'Mat Spec',
            'Mat Spec:',
            'Flange/Ring Gasket',
            'Communications protocol:',
        ]

        self.keyWordsSingleCol_datasheet = [

            'Communications protocol:',
        ]

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(750, 800)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.horizontalLayout_7 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_7.setObjectName("horizontalLayout_7")
        self.checkBox_3 = QtWidgets.QCheckBox(self.centralwidget)
        self.checkBox_3.setObjectName("checkBox_3")
        self.horizontalLayout_7.addWidget(self.checkBox_3)
        self.gridLayout.addLayout(self.horizontalLayout_7, 10, 0, 1, 3)
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setObjectName("pushButton")
        self.horizontalLayout_6.addWidget(self.pushButton)
        self.gridLayout.addLayout(self.horizontalLayout_6, 0, 0, 1, 3)
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setObjectName("label")
        self.verticalLayout_2.addWidget(self.label)
        self.listWidget = QtWidgets.QListWidget(self.centralwidget)
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(0, 195, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 195, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.PlaceholderText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 195, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 195, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.PlaceholderText, brush)
        brush = QtGui.QBrush(QtGui.QColor(120, 120, 120))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.PlaceholderText, brush)
        self.listWidget.setPalette(palette)
        self.listWidget.setObjectName("listWidget")
        item = QtWidgets.QListWidgetItem()
        self.listWidget.addItem(item)
        item = QtWidgets.QListWidgetItem()
        self.listWidget.addItem(item)
        item = QtWidgets.QListWidgetItem()
        self.listWidget.addItem(item)
        item = QtWidgets.QListWidgetItem()
        self.listWidget.addItem(item)
        item = QtWidgets.QListWidgetItem()
        self.listWidget.addItem(item)
        item = QtWidgets.QListWidgetItem()
        self.listWidget.addItem(item)
        item = QtWidgets.QListWidgetItem()
        self.listWidget.addItem(item)
        item = QtWidgets.QListWidgetItem()
        self.listWidget.addItem(item)
        item = QtWidgets.QListWidgetItem()
        self.listWidget.addItem(item)
        item = QtWidgets.QListWidgetItem()
        self.listWidget.addItem(item)
        item = QtWidgets.QListWidgetItem()
        self.listWidget.addItem(item)
        self.verticalLayout_2.addWidget(self.listWidget)
        self.horizontalLayout_5.addLayout(self.verticalLayout_2)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setObjectName("label_2")
        self.verticalLayout.addWidget(self.label_2)
        self.listWidget_2 = QtWidgets.QListWidget(self.centralwidget)
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(170, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(170, 0, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.PlaceholderText, brush)
        brush = QtGui.QBrush(QtGui.QColor(170, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(170, 0, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.PlaceholderText, brush)
        brush = QtGui.QBrush(QtGui.QColor(120, 120, 120))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.PlaceholderText, brush)
        self.listWidget_2.setPalette(palette)
        font = QtGui.QFont()
        font.setStrikeOut(True)
        self.listWidget_2.setFont(font)
        self.listWidget_2.setObjectName("listWidget_2")
        item = QtWidgets.QListWidgetItem()
        self.listWidget_2.addItem(item)
        item = QtWidgets.QListWidgetItem()
        self.listWidget_2.addItem(item)
        self.verticalLayout.addWidget(self.listWidget_2)
        self.horizontalLayout_5.addLayout(self.verticalLayout)
        self.gridLayout.addLayout(self.horizontalLayout_5, 1, 0, 1, 3)
        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(0, 165, 180))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 255, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 170, 127))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.ToolTipText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 255, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.PlaceholderText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 165, 180))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 255, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 170, 127))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.ToolTipText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 255, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.PlaceholderText, brush)
        brush = QtGui.QBrush(QtGui.QColor(120, 120, 120))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(120, 120, 120))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 170, 127))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.ToolTipText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.PlaceholderText, brush)
        self.label_5.setPalette(palette)
        self.label_5.setObjectName("label_5")
        self.gridLayout.addWidget(self.label_5, 13, 0, 1, 3)
        spacerItem = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        self.gridLayout.addItem(spacerItem, 14, 1, 1, 1)
        spacerItem1 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        self.gridLayout.addItem(spacerItem1, 12, 0, 1, 1)
        self.label_21 = QtWidgets.QLabel(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_21.sizePolicy().hasHeightForWidth())
        self.label_21.setSizePolicy(sizePolicy)
        self.label_21.setText("")
        self.label_21.setPixmap(QtGui.QPixmap("logoSiemens.png"))
        self.label_21.setScaledContents(False)
        self.label_21.setAlignment(QtCore.Qt.AlignCenter)
        self.label_21.setObjectName("label_21")
        self.gridLayout.addWidget(self.label_21, 15, 0, 1, 3)
        spacerItem2 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        self.gridLayout.addItem(spacerItem2, 8, 2, 1, 1)
        self.line = QtWidgets.QFrame(self.centralwidget)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.gridLayout.addWidget(self.line, 4, 0, 1, 3)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setObjectName("label_3")
        self.horizontalLayout_3.addWidget(self.label_3)
        self.lineEdit_4 = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_4.setObjectName("lineEdit_4")
        self.horizontalLayout_3.addWidget(self.lineEdit_4)
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setObjectName("label_4")
        self.horizontalLayout_3.addWidget(self.label_4)
        self.lineEdit_5 = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_5.setObjectName("lineEdit_5")
        self.horizontalLayout_3.addWidget(self.lineEdit_5)
        self.pushButton_5 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_5.setObjectName("pushButton_5")
        self.horizontalLayout_3.addWidget(self.pushButton_5)
        self.gridLayout.addLayout(self.horizontalLayout_3, 5, 0, 1, 3)
        spacerItem3 = QtWidgets.QSpacerItem(20, 10, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        self.gridLayout.addItem(spacerItem3, 6, 2, 1, 1)
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.pushButton_6 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_6.setObjectName("pushButton_6")
        self.horizontalLayout_4.addWidget(self.pushButton_6)
        self.pushButton_7 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_7.setObjectName("pushButton_7")
        self.horizontalLayout_4.addWidget(self.pushButton_7)
        self.pushButton_8 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_8.setObjectName("pushButton_8")
        self.horizontalLayout_4.addWidget(self.pushButton_8)
        self.pushButton_2 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_2.setObjectName("pushButton_2")
        self.horizontalLayout_4.addWidget(self.pushButton_2)
        self.pushButton_4 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_4.setObjectName("pushButton_4")
        self.horizontalLayout_4.addWidget(self.pushButton_4)
        self.gridLayout.addLayout(self.horizontalLayout_4, 2, 0, 1, 3)
        spacerItem4 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        self.gridLayout.addItem(spacerItem4, 3, 2, 1, 1)
        self.line_2 = QtWidgets.QFrame(self.centralwidget)
        self.line_2.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_2.setObjectName("line_2")
        self.gridLayout.addWidget(self.line_2, 9, 0, 1, 3)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.lineEdit = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit.setText("")
        self.lineEdit.setObjectName("lineEdit")
        self.horizontalLayout.addWidget(self.lineEdit)
        self.lineEdit_2 = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_2.setText("")
        self.lineEdit_2.setObjectName("lineEdit_2")
        self.horizontalLayout.addWidget(self.lineEdit_2)
        self.lineEdit_6 = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_6.setObjectName("lineEdit_6")
        self.horizontalLayout.addWidget(self.lineEdit_6)
        self.gridLayout.addLayout(self.horizontalLayout, 7, 0, 1, 3)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.pushButton_3 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_3.setObjectName("pushButton_3")
        self.horizontalLayout_2.addWidget(self.pushButton_3)
        self.gridLayout.addLayout(self.horizontalLayout_2, 11, 0, 1, 3)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 566, 21))
        self.menubar.setObjectName("menubar")
        self.menuManual_marking = QtWidgets.QMenu(self.menubar)
        self.menuManual_marking.setObjectName("menuManual_marking")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.actionSDP_X = QtWidgets.QAction(MainWindow)
        self.actionSDP_X.setObjectName("actionSDP_X")
        self.actionSDP_6 = QtWidgets.QAction(MainWindow)
        self.actionSDP_6.setObjectName("actionSDP_6")
        self.actionWEPS_OTHER = QtWidgets.QAction(MainWindow)
        self.actionWEPS_OTHER.setObjectName("actionWEPS_OTHER")
        self.actionSIMULATOR = QtWidgets.QAction(MainWindow)
        self.actionSIMULATOR.setObjectName("actionSIMULATOR")
        self.menuManual_marking.addAction(self.actionSDP_X)
        self.menuManual_marking.addAction(self.actionSDP_6)
        self.menuManual_marking.addAction(self.actionWEPS_OTHER)
        self.menuManual_marking.addAction(self.actionSIMULATOR)
        self.menubar.addAction(self.menuManual_marking.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Marking Generator"))
        self.checkBox_3.setText(_translate("MainWindow", "Generate PDF instead of .XLSX"))
        self.pushButton.setText(_translate("MainWindow", "Scan specification matrix "))
        self.label.setText(_translate("MainWindow", "Included (Double click to exclude)"))
        __sortingEnabled = self.listWidget.isSortingEnabled()
        self.listWidget.setSortingEnabled(False)
        item = self.listWidget.item(0)
        item.setText(_translate("MainWindow", "Customer:    Dana Petroleum\n"
                                              ""))
        item = self.listWidget.item(1)
        item.setText(_translate("MainWindow", "Customer PO.:    616452\n"
                                              ""))
        item = self.listWidget.item(2)
        item.setText(_translate("MainWindow", "GA Drwg :    10168417\n"
                                              ""))
        item = self.listWidget.item(3)
        item.setText(_translate("MainWindow", "Probe  length :    65mm\n"
                                              ""))
        item = self.listWidget.item(4)
        item.setText(_translate("MainWindow", "Max test pressure: 1035 Bar\n"
                                              ""))
        item = self.listWidget.item(5)
        item.setText(_translate("MainWindow", "Cal. Pressure range :0-345 Bara\n"
                                              ""))
        item = self.listWidget.item(6)
        item.setText(_translate("MainWindow", "Mat Spec:API 6A/17D. PSL 3 PR2\n"
                                              ""))
        item = self.listWidget.item(7)
        item.setText(_translate("MainWindow", "Mat.Temp. Class: HH  / LU\n"
                                              ""))
        item = self.listWidget.item(8)
        item.setText(_translate("MainWindow", "Flange/Ring Gasket: API 6A 2 1/16” 10K BX-152\n"
                                              ""))
        item = self.listWidget.item(9)
        item.setText(_translate("MainWindow", "Date of Manufacture: xx.xx.xxxx\n"
                                              ""))
        item = self.listWidget.item(10)
        item.setText(_translate("MainWindow", "New Item"))
        self.listWidget.setSortingEnabled(__sortingEnabled)
        self.label_2.setText(_translate("MainWindow", "Excluded (Double click to include)"))
        __sortingEnabled = self.listWidget_2.isSortingEnabled()
        self.listWidget_2.setSortingEnabled(False)
        item = self.listWidget_2.item(0)
        item.setText(_translate("MainWindow", "Customer PN :    \n"
                                              "NA"))
        item = self.listWidget_2.item(1)
        item.setText(_translate("MainWindow", "Cal. Temp. range:\n"
                                              "NA"))
        self.listWidget_2.setSortingEnabled(__sortingEnabled)
        self.label_5.setText(_translate("MainWindow", "Console: "))
        self.label_3.setText(_translate("MainWindow", "Label"))
        self.lineEdit_4.setPlaceholderText(_translate("MainWindow", "Enter the name of an additional optional line"))
        self.label_4.setText(_translate("MainWindow", "value"))
        self.lineEdit_5.setPlaceholderText(_translate("MainWindow", "Enter the corresponding value"))
        self.pushButton_5.setText(_translate("MainWindow", "Add"))
        self.pushButton_6.setText(_translate("MainWindow", "Move item up"))
        self.pushButton_7.setText(_translate("MainWindow", "Move item down"))
        self.pushButton_8.setText(_translate("MainWindow", "Edit"))
        self.pushButton_2.setText(_translate("MainWindow", "Previous part"))
        self.pushButton_4.setText(_translate("MainWindow", "Next part"))
        self.lineEdit.setPlaceholderText(_translate("MainWindow", "Enter Serial number  (optional)"))
        self.lineEdit_2.setPlaceholderText(_translate("MainWindow", "Enter date of prodution (Optional)"))
        self.lineEdit_6.setPlaceholderText(_translate("MainWindow", "Enter PO number (optional)"))
        self.pushButton_3.setText(_translate("MainWindow", "Generate marking"))
        self.menuManual_marking.setTitle(_translate("MainWindow", "Manual marking"))
        self.actionSDP_X.setText(_translate("MainWindow", "SDP X"))
        self.actionSDP_6.setText(_translate("MainWindow", "SDP-6"))
        self.actionWEPS_OTHER.setText(_translate("MainWindow", "WEPS/OTHER"))
        self.actionSIMULATOR.setText(_translate("MainWindow", "SIMULATOR"))


